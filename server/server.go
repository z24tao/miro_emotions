package server

import (
	"../database"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
)

func Start() {
	http.Handle("/", noDirListing(http.FileServer(http.Dir("server/static"))))
	http.HandleFunc("/comment", commentHandler)
	http.HandleFunc("/complete", completeHandler)
	http.HandleFunc("/create", createHandler)
	http.HandleFunc("/select", selectHandler)
	http.HandleFunc("/subject", subjectHandler)
	http.HandleFunc("/understanding", understandingHandler)
	http.HandleFunc("/demographics", demographicsHandler)
	http.HandleFunc("/replay", replayHandler)
	http.HandleFunc("/watch_again", watchAgainHandler)
	http.HandleFunc("/idaq", idaqHandler)
	http.HandleFunc("/picture", pictureHandler)
	http.HandleFunc("/gender", genderHandler)
	fmt.Println("server started ...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func noDirListing(h http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r)
	})
}

func getUrlParam(r *http.Request, name string) string {
	if name == "" {
		return ""
	}
	query := r.URL.Query()
	params := query[name]
	if len(params) == 0 || params[0] == "" {
		return ""
	}
	return params[0]
}

func getUrlBoolParam(r *http.Request, name string) bool {
	return getUrlParam(r, name) == "1"
}

func getUrlIntParam(r *http.Request, name string) int {
	str := getUrlParam(r, name)
	val, err := strconv.Atoi(str)
	reportError(err)
	return val
}

func createHandler(w http.ResponseWriter, r *http.Request) {
	mturkId := getUrlParam(r, "mturk_id")
	hitId := getUrlParam(r, "hit_id")
	sessionId := database.CreateUser(mturkId, hitId)
	_, err := w.Write([]byte(sessionId))
	reportError(err)
}

func subjectHandler(w http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")

	if database.CountCannotPlay(sessionId) >= 2 {
		_, err := w.Write([]byte("cannot_play"))
		reportError(err)
		return
	}

	subject := database.GetSubject(sessionId)
	orderNum := len(database.GetCompletedSubjects(sessionId)) + 1

	if subject == "" {
		subjectListCopy := make([]string, 0)
		completedSubjects := database.GetCompletedSubjects(sessionId)

		for _, subject := range subjectList {
			if !completedSubjects[subject] {
				subjectListCopy = append(subjectListCopy, subject)
			}
		}

		if len(subjectListCopy) == 0 {
			_, err := w.Write([]byte("completed"))
			reportError(err)
			return
		}

		random := rand.Intn(len(subjectListCopy))
		subject = subjectListCopy[random]
		fmt.Println(random, subject)
		database.UpdateSubject(sessionId, subject)
	}

	_, err := w.Write([]byte(subject + "_" + strconv.Itoa(orderNum)))
	fmt.Println(subject + "_" + strconv.Itoa(orderNum))
	reportError(err)
}

func selectHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")
	subject := getUrlParam(r, "subject")

	cannotPlay := getUrlBoolParam(r, "cannot_play")

	options := make(map[string]bool)
	options["bored"] = getUrlBoolParam(r, "bored")
	options["surprise"] = getUrlBoolParam(r, "surprise")
	options["disgust"] = getUrlBoolParam(r, "disgust")
	options["tired"] = getUrlBoolParam(r, "tired")
	options["fear"] = getUrlBoolParam(r, "fear")
	options["excited"] = getUrlBoolParam(r, "excited")
	options["happy"] = getUrlBoolParam(r, "happy")
	options["annoyed"] = getUrlBoolParam(r, "annoyed")
	options["angry"] = getUrlBoolParam(r, "angry")
	options["calm"] = getUrlBoolParam(r, "calm")
	options["sad"] = getUrlBoolParam(r, "sad")
	options["not_sure"] = getUrlBoolParam(r, "not_sure")

	if !database.EntryExists(sessionId, subject) {
		database.CreateEntry(sessionId, subject)
	}
	if cannotPlay {
		database.CannotPlay(sessionId, subject)
	}

	database.UpdateEntry(sessionId, subject, options)
}

func commentHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")
	subject := getUrlParam(r, "subject")
	comment := getUrlParam(r, "comment")

	database.UpdateComment(sessionId, subject, comment)
}

func completeHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")

	database.UpdateSubject(sessionId, "")
}

func pictureHandler(w http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")

	pictureListCopy := make([]string, 0)
	completedSubjects := database.GetCompletedPictures(sessionId)

	for _, picture := range pictureList {
		if !completedSubjects[picture] {
			pictureListCopy = append(pictureListCopy, picture)
		}
	}

	if len(pictureListCopy) == 0 {
		_, err := w.Write([]byte("completed"))
		reportError(err)
		return
	}

	random := rand.Intn(len(pictureListCopy))
	picture := pictureListCopy[random]

	_, err := w.Write([]byte(picture + "_" + strconv.Itoa(len(completedSubjects) + 1)))
	reportError(err)
}

func understandingHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")
	picture := getUrlParam(r, "picture")

	options := make(map[string]bool)
	options["angry"] = getUrlBoolParam(r, "angry")
	options["bored"] = getUrlBoolParam(r, "bored")
	options["fear"] = getUrlBoolParam(r, "fear")
	options["happy"] = getUrlBoolParam(r, "happy")
	options["neutral"] = getUrlBoolParam(r, "neutral")
	options["sad"] = getUrlBoolParam(r, "sad")
	options["surprise"] = getUrlBoolParam(r, "surprise")
	options["not_sure"] = getUrlBoolParam(r, "not_sure")

	database.CreateUnderstanding(sessionId, picture, options)
}

func demographicsHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")

	optionsStr := make(map[string]string)
	optionsStr["gender"] = getUrlParam(r, "gender")
	optionsStr["education"] = getUrlParam(r, "education")
	optionsStr["ethnicity"] = getUrlParam(r, "ethnicity")
	optionsStr["miro_animal"] = getUrlParam(r, "miro_animal")
	optionsStr["pet_type_self"] = getUrlParam(r, "pet_type_self")
	optionsStr["pet_type_friend"] = getUrlParam(r, "pet_type_friend")

	optionsInt := make(map[string]int)
	optionsInt["age"] = getUrlIntParam(r, "age")
	optionsInt["dog_cat"] = getUrlIntParam(r, "dog_cat")
	optionsInt["like_pet"] = getUrlIntParam(r, "like_pet")
	optionsInt["scared"] = getUrlIntParam(r, "scared")
	optionsInt["pet_emotion"] = getUrlIntParam(r, "pet_emotion")
	optionsInt["pet_emotion_difficulty"] = getUrlIntParam(r, "pet_emotion_difficulty")
	optionsInt["animal_emotion"] = getUrlIntParam(r, "animal_emotion")
	optionsInt["human_like"] = getUrlIntParam(r, "human_like")
	optionsInt["animal_like"] = getUrlIntParam(r, "animal_like")
	optionsInt["machine_like"] = getUrlIntParam(r, "machine_like")

	optionsBool := make(map[string]bool)
	optionsBool["has_pet_self"] = getUrlBoolParam(r, "has_pet_self")
	optionsBool["has_pet_friend"] = getUrlBoolParam(r, "has_pet_friend")

	database.CreateDemographics(sessionId, optionsStr, optionsInt, optionsBool)
}

func idaqHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")

	options := make(map[string]int)
	options["qq1"] = getUrlIntParam(r, "qq1")
	options["qq2"] = getUrlIntParam(r, "qq2")
	options["qq3"] = getUrlIntParam(r, "qq3")
	options["qq4"] = getUrlIntParam(r, "qq4")
	options["qq5"] = getUrlIntParam(r, "qq5")
	options["qq6"] = getUrlIntParam(r, "qq6")
	options["qq7"] = getUrlIntParam(r, "qq7")
	options["qq8"] = getUrlIntParam(r, "qq8")
	options["qq9"] = getUrlIntParam(r, "qq9")
	options["qq10"] = getUrlIntParam(r, "qq10")
	options["qq11"] = getUrlIntParam(r, "qq11")
	options["qq12"] = getUrlIntParam(r, "qq12")
	options["qq13"] = getUrlIntParam(r, "qq13")
	options["qq14"] = getUrlIntParam(r, "qq14")
	options["qq15"] = getUrlIntParam(r, "qq15")
	options["qq16"] = getUrlIntParam(r, "qq16")
	options["qq17"] = getUrlIntParam(r, "qq17")
	options["qq18"] = getUrlIntParam(r, "qq18")

	database.CreateIdaq(sessionId, options)
}

func genderHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")

	gender := "neither"
	if getUrlBoolParam(r, "dmale") {
		gender = "definitely male"
	} else if getUrlBoolParam(r, "pmale") {
		gender = "probably male"
	} else if getUrlBoolParam(r, "dfemale") {
		gender = "definitely female"
	} else if getUrlBoolParam(r, "pfemale") {
		gender = "probably female"
	} else if getUrlBoolParam(r, "both") {
		gender = "both"
	}

	database.UpdateGender(sessionId, gender)
}

func replayHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")
	subject := getUrlParam(r, "subject")

	if !database.EntryExists(sessionId, subject) {
		database.CreateEntry(sessionId, subject)
	}

	database.AddReplay(sessionId, subject)
}

func watchAgainHandler(_ http.ResponseWriter, r *http.Request) {
	sessionId := getUrlParam(r, "session_id")
	subject := getUrlParam(r, "subject")

	if !database.EntryExists(sessionId, subject) {
		database.CreateEntry(sessionId, subject)
	}

	database.AddWatchAgain(sessionId, subject)
}

var subjectHappy = "happy"
var subjectSad = "sad"
var subjectExcited = "excited"
var subjectFearful = "fear"
var subjectAngry = "angry"
var subjectDisgusted = "disgust"
var subjectBored = "bored"
var subjectAnnoyed = "annoyed"
var subjectCalm = "calm"
var subjectTired = "tired"
var subjectSurprise = "surprise"

var subjectList = []string {
	subjectHappy,
	subjectSad,
	subjectExcited,
	subjectFearful,
	subjectAngry,
	subjectDisgusted,
	subjectBored,
	subjectAnnoyed,
	subjectCalm,
	subjectTired,
	subjectSurprise,
}

var pictureList = []string {
	"angry1",
	"angry2",
	"disgust1",
	"disgust2",
	"fear1",
	"fear2",
	"happy1",
	"happy2",
	"neutral1",
	"neutral2",
	"sad1",
	"sad2",
	"surprise1",
	"surprise2",
}

func reportError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
